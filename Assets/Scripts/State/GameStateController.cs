﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class GameStateController
{

  private enum State
  {
    TERMINATED,
    FROM_TERMINATED_TO_GAME,
    IN_DA_GAME,
    MAIN_MENU,
    PAUSED,
    DEFEAT,
    VICTORY,
    MAIN_MENU_LOADING
  }

  public enum Command
  {
    BACK,
    CONTINUE,
    DEFEAT,
    WIN,
    LOADED,
    QUIT
  }

  private class Transition
  {
    private readonly State currentState;
    private readonly Command command;

    public Transition(State currentState, Command command) {
      this.currentState = currentState;
      this.command = command;
    }

    public override int GetHashCode() {
      return 17 + 31 * currentState.GetHashCode() + 31 * command.GetHashCode();
    }

    public override bool Equals(object obj) {
      Transition other = obj as Transition;
      return other != null && currentState == other.currentState && command == other.command;
    }
  }
  
  private State CurrentState = State.TERMINATED;
  private Dictionary<Transition, State> transitions;

  public void Init() {

    transitions = new Dictionary<Transition, State> {
      {new Transition(State.TERMINATED, Command.LOADED), State.MAIN_MENU},
      
      {new Transition(State.FROM_TERMINATED_TO_GAME, Command.LOADED), State.MAIN_MENU},
      
      {new Transition(State.IN_DA_GAME, Command.QUIT), State.TERMINATED},
      {new Transition(State.IN_DA_GAME, Command.BACK), State.PAUSED},
      {new Transition(State.IN_DA_GAME, Command.DEFEAT), State.DEFEAT},
      {new Transition(State.IN_DA_GAME, Command.WIN), State.VICTORY},

      {new Transition(State.PAUSED, Command.QUIT), State.TERMINATED},
      {new Transition(State.PAUSED, Command.CONTINUE), State.MAIN_MENU_LOADING},
      {new Transition(State.PAUSED, Command.BACK), State.IN_DA_GAME},

      {new Transition(State.DEFEAT, Command.QUIT), State.TERMINATED},
      {new Transition(State.DEFEAT, Command.CONTINUE), State.MAIN_MENU_LOADING},
      {new Transition(State.DEFEAT, Command.BACK), State.MAIN_MENU_LOADING},
      {new Transition(State.DEFEAT, Command.LOADED), State.IN_DA_GAME},
      
      {new Transition(State.VICTORY, Command.QUIT), State.TERMINATED},
      {new Transition(State.VICTORY, Command.CONTINUE), State.MAIN_MENU_LOADING},
      {new Transition(State.VICTORY, Command.BACK), State.MAIN_MENU_LOADING},
      {new Transition(State.VICTORY, Command.LOADED), State.IN_DA_GAME},
      {new Transition(State.VICTORY, Command.WIN), State.VICTORY}, // bug fix
      {new Transition(State.VICTORY, Command.DEFEAT), State.VICTORY}, // bug fix
      
      {new Transition(State.MAIN_MENU_LOADING, Command.LOADED), State.MAIN_MENU},
      
      {new Transition(State.MAIN_MENU, Command.LOADED), State.IN_DA_GAME},
    };
    
  }
  
  public void TransitState(Command command) {
    
    State nextState = GetNextState(command);
    
    Debug.Log("Game state transition: " + CurrentState + " -> " + command + " -> " + nextState);
    
    //if(State != State.UNKNOWN)
    ApplyState(nextState);
    CurrentState = nextState;
  }
  
  private State GetNextState(Command command)
  {
    Transition transition = new Transition(CurrentState, command);
    State nextState;
    if (!transitions.TryGetValue(transition, out nextState)) {
      Debug.LogWarning("Invalid transition: " + CurrentState + " -> " + command);
    }

    return nextState;
  }
  

  private void ApplyState(State state) {
    switch (state) {
      case State.IN_DA_GAME:
        GameController.Instance.Continue();
        break;
      
      case State.PAUSED:
        GameController.Instance.Pause();
        break;
      
      case State.MAIN_MENU:
        
        break;
      
      case State.MAIN_MENU_LOADING:
        AppController.Instance.AppStateController.TransitState(AppStateController.Command.LOADED);
        break;
      
      case State.DEFEAT:
        GameController.Instance.Defeat();
        break;
      
      case State.VICTORY:
        GameController.Instance.Win();
        break;
      
      case State.TERMINATED:
        AppController.Instance.AppStateController.TransitState(AppStateController.Command.QUIT);
        break;
    }
  }

}