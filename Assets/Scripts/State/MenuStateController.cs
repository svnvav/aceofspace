﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class MenuStateController
{

  private enum State
  {
    TERMINATED,
    MAIN_MENU,
    CHOOSE_SHIP,
    CHOOSE_LEVEL,
    GAME_LOADING,
    IN_DA_GAME
  }

  public enum Command
  {
    BACK,
    QUIT,
    CHOOSE_SHIP,
    CHOOSE_LEVEL,
    PLAY,
    LOADED
  }

  private class Transition
  {
    private readonly State currentState;
    private readonly Command command;

    public Transition(State currentState, Command command) {
      this.currentState = currentState;
      this.command = command;
    }

    public override int GetHashCode() {
      return 17 + 31 * currentState.GetHashCode() + 31 * command.GetHashCode();
    }

    public override bool Equals(object obj) {
      Transition other = obj as Transition;
      return other != null && currentState == other.currentState && command == other.command;
    }
  }
  
  private State CurrentState = State.TERMINATED;
  private Dictionary<Transition, State> transitions;

  public void Init() {

    transitions = new Dictionary<Transition, State> {
      {new Transition(State.TERMINATED, Command.LOADED), State.MAIN_MENU},
      
      {new Transition(State.MAIN_MENU, Command.PLAY), State.GAME_LOADING},
      {new Transition(State.MAIN_MENU, Command.CHOOSE_SHIP), State.CHOOSE_SHIP},
      {new Transition(State.MAIN_MENU, Command.CHOOSE_LEVEL), State.CHOOSE_LEVEL},
      {new Transition(State.MAIN_MENU, Command.QUIT), State.TERMINATED},

      {new Transition(State.CHOOSE_SHIP, Command.BACK), State.MAIN_MENU},
      {new Transition(State.CHOOSE_SHIP, Command.QUIT), State.TERMINATED},
      
      {new Transition(State.CHOOSE_LEVEL, Command.BACK), State.MAIN_MENU},
      {new Transition(State.CHOOSE_LEVEL, Command.QUIT), State.TERMINATED},

      {new Transition(State.GAME_LOADING, Command.LOADED), State.IN_DA_GAME},
      
      {new Transition(State.IN_DA_GAME, Command.LOADED), State.MAIN_MENU},
    };
    
  }
  
  public void TransitState(Command command) {
    
    State nextState = GetNextState(command);
    
    Debug.Log("Menu state transition: " + CurrentState + " -> " + command + " -> " + nextState);
    
    ApplyState(nextState);
    CurrentState = nextState;
  }
  
  private State GetNextState(Command command)
  {
    Transition transition = new Transition(CurrentState, command);
    State nextState;
    if (!transitions.TryGetValue(transition, out nextState))
      Debug.LogWarning("Invalid transition: " + CurrentState + " -> " + command);
    return nextState;
  }

  private void ApplyState(State state) {
    switch (state) {
      case State.MAIN_MENU:
        MainMenuController.Instance.GoToMainPanel();
        break;
      
      case State.CHOOSE_SHIP:
        MainMenuController.Instance.GoToChooseShipPanel();
        break;
      
      case State.CHOOSE_LEVEL:
        MainMenuController.Instance.GoToChooseLevelPanel();
        break;
      
      case State.GAME_LOADING:
        AppController.Instance.AppStateController.TransitState(AppStateController.Command.LOADED);
        break;
      
      case State.IN_DA_GAME:
        
        break;
      
      case State.TERMINATED:
        AppController.Instance.AppStateController.TransitState(AppStateController.Command.QUIT);
        break;
    }
  }
}