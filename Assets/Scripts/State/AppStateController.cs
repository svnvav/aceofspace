﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class AppStateController
{

  private enum State
  {
    TERMINATED,
    MENU,
    GAME
  }

  public enum Command
  { 
    QUIT,
    LOADED
  }

  private class Transition
  {
    private readonly State currentState;
    private readonly Command command;

    public Transition(State currentState, Command command) {
      this.currentState = currentState;
      this.command = command;
    }

    public override int GetHashCode() {
      return 17 + 31 * currentState.GetHashCode() + 31 * command.GetHashCode();
    }

    public override bool Equals(object obj) {
      Transition other = obj as Transition;
      return other != null && currentState == other.currentState && command == other.command;
    }
  }
  
  private State currentState = State.TERMINATED;
  private Dictionary<Transition, State> transitions;

  public void Init() {

    transitions = new Dictionary<Transition, State> {
      {new Transition(State.TERMINATED, Command.LOADED), State.MENU},
      
      {new Transition(State.MENU, Command.LOADED), State.GAME},
      {new Transition(State.MENU, Command.QUIT), State.TERMINATED},
      
      {new Transition(State.GAME, Command.LOADED), State.MENU},
      {new Transition(State.GAME, Command.QUIT), State.TERMINATED}
    };
    
  }
  
  public void TransitState(Command command) {

    State nextState = GetNextState(command);
    Debug.Log("______________________________________________________________________________");
    Debug.Log("App state transition: " + currentState + " -> " + command + " -> " + nextState);
    
    ApplyState(nextState);
    currentState = nextState;
  }
  
  private State GetNextState(Command command)
  {
    Transition transition = new Transition(currentState, command);
    State nextState;
    if (!transitions.TryGetValue(transition, out nextState))
      Debug.LogWarning("Invalid transition: " + currentState + " -> " + command);
    return nextState;
  }
  

  private void ApplyState(State state) {
    
    switch (state) {
      case State.MENU:
        AppController.Instance.GoToMainMenu();
        break;
      
      case State.GAME:
        AppController.Instance.GoToGame();
        break;
      
      case State.TERMINATED:
        AppController.Instance.Quit();
        break;
    }

    
  }
}