﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BonusSettings
{
  public string Name;
  public GameObject Prefab;

  public BonusSettings() {
    Name = "Bonus";
  }
}

public class BonusesConfig : ScriptableObject
{
  public List<BonusSettings> Bonuses;
}