﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BaseShipStats
{
  public string Type;
  public int Health;
  public int ShootSpeed;
  public int MoveSpeed;
  public int Mobility;
  
  public BaseShipStats() {
    Type = "Type a type";
    Health = 0;
    ShootSpeed = 0;
    MoveSpeed = 0;
    Mobility = 0;
  }
}

[System.Serializable]
public class ShipVariation
{
  public int TypeId;
  public string Id;
  public GameObject BodyPrefab;
  public float HealthMultiplier;
  public float ShootSpeedMultiplier;
  public float MoveSpeedMultiplier;
  public float MobilityMultiplier;
  public float WeaponDamageMultiplier;
  
  public ShipVariation() {
    TypeId = 0;
    Id = "(Id)";
    HealthMultiplier = 1;
    ShootSpeedMultiplier = 1;
    MoveSpeedMultiplier = 1;
    MobilityMultiplier = 1;
    WeaponDamageMultiplier = 1;
  }
}

[System.Serializable]
public class ShipsConfig : ScriptableObject
{
  public List<BaseShipStats> BaseStats;

  public List<ShipVariation> ShipsVariations;
  
  //TODO: implement add and remove functions and refactor Editor/ShipStatsManager.cs
}