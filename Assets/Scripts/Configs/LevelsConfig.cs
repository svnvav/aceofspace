﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelSettings
{
  public string Name;
  public GameObject Background;
  public AudioClip AudioClip;
  public List<SquadConfig> Waves;

  public LevelSettings() {
    Name = "Level name";
    Waves = new List<SquadConfig>();
  }
}

public class LevelsConfig : ScriptableObject
{
  public List<LevelSettings> Levels;
}