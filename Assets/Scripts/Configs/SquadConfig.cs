﻿using UnityEngine;

[System.Serializable]
public class SquadPosition
{
  public Vector3 Position;
  public int UnitVariationId;
}

public class SquadConfig : ScriptableObject
{
  public SquadPosition[] Positions;
}