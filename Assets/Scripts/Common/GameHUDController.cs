﻿using UnityEngine;
using UnityEngine.UI;

public class GameHUDController : MonoBehaviour
{
  public static GameHUDController Instance;
  
  public GameObject Veil;
  public GameObject PauseMenu;
  public GameObject DefeatPanel;
  public GameObject VictoryPanel;
  public Text VictoryScore;
  public Text VictoryLevelRecordScore;
  public Text VictoryTotalScore;
  public GameObject VictoryNextLevelButton;
  
  public Text Score;
  public Slider PlayerHealthsBar;
  public Slider ShootSpeedBonusTimer;
  public Slider ShieldBonusTimer;
  public Slider DamageBonusTimer;

  private void Awake() {
    if (!Instance) Instance = this;
  }

  private void Start() {
    SetHitPointsValues(GameController.Instance.Player.Stats.MaxHealth, GameController.Instance.Player.Stats.CurrentHealth);
  }

  public void SetHitPointsValues(int max, int current) {
    PlayerHealthsBar.value = (float)current / max;
  }

  public void SetScoreValue(int value) {
    Score.text = "Score: " + value;
  }
  
  public void ShowPausePanel() {  
    Veil.SetActive(true);
    PauseMenu.SetActive(true);
  }
  
  public void Continue() {  
    Veil.SetActive(false);
    PauseMenu.SetActive(false);
    DefeatPanel.SetActive(false);
    VictoryPanel.SetActive(false);
  }
  
  public void ShowDefeatPanel() {  
    Veil.SetActive(true);
    DefeatPanel.SetActive(true);
  }
  
  public void ShowVictoryPanel() {
    VictoryScore.text = "Score: " + GameController.Instance.Score;
    VictoryLevelRecordScore.text = "Level record: " + DataController.Instance.GetLevelScore(DataController.Instance.CurrentLevelIndex);
    VictoryTotalScore.text = "Total score: " + DataController.Instance.TotalScore;
    VictoryNextLevelButton.SetActive(DataController.Instance.CurrentLevelIndex + 1 < DataController.Instance.LevelsConfig.Levels.Count);
    Veil.SetActive(true);
    VictoryPanel.SetActive(true);
  }

  public void HandlePause() {
    AppController.Instance.GameStateController.TransitState(GameStateController.Command.BACK);
  }
  
  public void HandleNextLevel() {
    GameController.Instance.StartLevel(++DataController.Instance.CurrentLevelIndex);
    DataController.Instance.SaveCurrentLevel();
    AppController.Instance.GameStateController.TransitState(GameStateController.Command.LOADED);
  }
  
  public void HandleRepeat() {
    GameController.Instance.StartLevel(DataController.Instance.CurrentLevelIndex);
    AppController.Instance.GameStateController.TransitState(GameStateController.Command.LOADED);
  }

  public void HandleContinue() {
    AppController.Instance.GameStateController.TransitState(GameStateController.Command.BACK);
  }
  
  public void HandleMainMenu() {
    AppController.Instance.GameStateController.TransitState(GameStateController.Command.CONTINUE);
  }
  
  public void HandleClose() {
    AppController.Instance.GameStateController.TransitState(GameStateController.Command.BACK);
  }
  
  public void HandleQuit() {
    AppController.Instance.Quit();
  }
}