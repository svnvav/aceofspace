﻿using UnityEngine;
using UnityEngine.Animations;

public class Rotator : MonoBehaviour
{
  public float RotationSpeed;
  public Axis Axis;

  private void Update() {
    transform.Rotate(
      Axis == Axis.X ? RotationSpeed * Time.deltaTime : 0, 
      Axis == Axis.Y ? RotationSpeed * Time.deltaTime : 0, 
      Axis == Axis.Z ? RotationSpeed * Time.deltaTime : 0
    );
  }
}