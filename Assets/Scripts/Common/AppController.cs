﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AppController : MonoBehaviour
{
  public static AppController Instance;

  public AppStateController AppStateController;
  public MenuStateController MenuStateController;
  public GameStateController GameStateController;
  
  private void Awake() {
    if (!Instance) Instance = this;
    DontDestroyOnLoad(this);
  }
  
  private void Start() {  
    MenuStateController = new MenuStateController();
    MenuStateController.Init();
    
    GameStateController = new GameStateController();
    GameStateController.Init();  
    
    AppStateController = new AppStateController();
    AppStateController.Init();
    
    AppStateController.TransitState(AppStateController.Command.LOADED);
  }

  public void GoToMainMenu() {
    StartCoroutine(LoadMenuSceneAsynchronosly());
  }
  
  IEnumerator LoadMenuSceneAsynchronosly()
  {
    AsyncOperation operation = SceneManager.LoadSceneAsync("Menu");
      
    while (!operation.isDone)
    {
      /*float progress = operation.progress / .9f;
      loadImage.fillAmount = progress;
      progressText.text = string.Format("{0:0}", "LOADING " + progress * 100 + "%");*/

      yield return null;
    }
    GameStateController.TransitState(GameStateController.Command.LOADED);
    MenuStateController.TransitState(MenuStateController.Command.LOADED);
  }
  
  public void GoToGame() {
    StartCoroutine(LoadGameSceneAsynchronosly());
  } 
  
  IEnumerator LoadGameSceneAsynchronosly()
  {
    AsyncOperation operation = SceneManager.LoadSceneAsync("Game");
      
    while (!operation.isDone)
    {
      /*float progress = operation.progress / .9f;
      loadImage.fillAmount = progress;
      progressText.text = string.Format("{0:0}", "LOADING " + progress * 100 + "%");*/

      yield return null;
    }
    GameStateController.TransitState(GameStateController.Command.LOADED);
    MenuStateController.TransitState(MenuStateController.Command.LOADED);
  }
  
  public void Quit() {
    Application.Quit();
  }
}