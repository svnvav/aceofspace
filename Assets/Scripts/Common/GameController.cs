﻿using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
  public static GameController Instance;

  public AudioSource AudioSource;
  public GameObject Background;

  public GameHUDController Hud;

  public int Score;
  public Player Player;
  private Vector3 playerStartPosition;
  private Quaternion playerStartRotation;

  public GameObject EnemyPrefab;
  public EnemiesSquad EnemiesSquad;

  private int nextWaveIndex;

  private void Awake() {
    if (!Instance) Instance = this;
  }

  private void Start() {    
    Player.SetBody(DataController.Instance.PlayerShipVariationIndex);
    playerStartPosition = Player.transform.position;
    playerStartRotation = Player.transform.rotation;
    EnemiesSquad.Units = new List<Enemy>();
    StartLevel(DataController.Instance.CurrentLevelIndex);
  }

  private void Update() {
    if(Input.GetKey(KeyCode.Escape))
      Pause();
  }

  private void SpawnWave(int index) {
    //EnemiesSquad.Clear();

    foreach (var position in DataController.Instance.LevelsConfig.Levels[DataController.Instance.CurrentLevelIndex]
      .Waves[index].Positions) {
      var enemy = Instantiate(EnemyPrefab, EnemiesSquad.transform, true).GetComponent<Enemy>();
      enemy.transform.localPosition = position.Position;
      enemy.transform.rotation = EnemiesSquad.transform.rotation;
      enemy.Stats = DataController.Instance.GetUnitStats(position.UnitVariationId);
      enemy.SetBody(position.UnitVariationId);
      EnemiesSquad.Units.Add(enemy);
      enemy.Squad = EnemiesSquad;
    }
    
    EnemiesSquad.Warp(9, 1);

    nextWaveIndex++;
  }

  /// <summary>
  /// Executes everytime when enemy ship was destroyed ot left game area.
  /// If there is no enemy in the game area it spawns new wave or finishes current level.
  /// </summary>
  public void CheckSquad() {
    if (EnemiesSquad.Units.Count > 0)
      return;

    if (nextWaveIndex >= DataController.Instance.LevelsConfig.Levels[DataController.Instance.CurrentLevelIndex].Waves.Count) {
      AppController.Instance.GameStateController.TransitState(GameStateController.Command.WIN);
      return;
    }

    SpawnWave(nextWaveIndex);
    
  }

  public void Pause() {
    Player.enabled = false;
    Hud.ShowPausePanel();
    Time.timeScale = 0.0f;
  }

  public void Continue() {
    Player.enabled = true;
    Hud.Continue();
    Time.timeScale = 1.0f;
  }

  public void Defeat() {
    Player.enabled = false;
    Hud.ShowDefeatPanel();
    Time.timeScale = 0.0f;
  }

  public void Win() {
    Player.enabled = false;
    DataController.Instance.SaveLevelScore(DataController.Instance.CurrentLevelIndex, Score);
    Hud.ShowVictoryPanel();
    Time.timeScale = 0.0f;
  }

  /// <summary>
  /// Reset all Game variables and spawn the first enemies wave
  /// </summary>
  public void StartLevel(int levelIndex) {
    AudioSource.clip = DataController.Instance.LevelsConfig.Levels[levelIndex].AudioClip;
    AudioSource.Play();
    
    BonusesController.Instance.StopAll();
    
    Player.gameObject.tag = "Player";
    Player.BodyTransform.gameObject.SetActive(true);
    Player.transform.position = playerStartPosition;
    Player.transform.rotation = playerStartRotation;
    Player.Stats = DataController.Instance.GetPlayerStats();
    GameHUDController.Instance.SetHitPointsValues(Player.Stats.MaxHealth, Player.Stats.CurrentHealth);
    
    Score = 0;
    Hud.SetScoreValue(Score);
    nextWaveIndex = 0;

    EnemiesSquad.Clear();
    
    ShotsPool.Instance.PushAllActiveChilds();
    
    SpawnWave(nextWaveIndex);
    if (Background != null) {
      Destroy(Background);
    }

    Background = Instantiate(DataController.Instance.LevelsConfig.Levels[levelIndex].Background);
  }
}