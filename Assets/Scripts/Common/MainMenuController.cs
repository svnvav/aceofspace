﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
  public static MainMenuController Instance;
  
  public GameObject MainPanel;
  public Text TotalScore;
  
  public GameObject ChooseShipPanel;
  public Transform ChooseShipPanelContent;
  private Button[] chooseShipPanelItems;
  
  public GameObject ChooseLevelPanel;
  public Transform ChooseLevelPanelContent;
  private Button[] chooseLevelPanelItems;
  public GameObject Background;

  public GameObject MenuItemPrefab;

  public Transform PlayerShipVariationTransform;

  private void Awake() {
    if (!Instance) Instance = this;
    Time.timeScale = 1;
  }

  private void OnEnable() {
    
    CofigureChooseShipPanelButtons();
    SetPlayerShipVariation(DataController.Instance.PlayerShipVariationIndex);
    CofigureChooseLevelPanelButtons();
    SetLevel(DataController.Instance.CurrentLevelIndex);
    SetTotalSceore();
  }

  public void HandlePlay() {
    AppController.Instance.MenuStateController.TransitState(MenuStateController.Command.PLAY);
  }
  
  public void HandleChooseShip() {
    AppController.Instance.MenuStateController.TransitState(MenuStateController.Command.CHOOSE_SHIP);
  }
  
  public void HandleChooseLevel() {
    AppController.Instance.MenuStateController.TransitState(MenuStateController.Command.CHOOSE_LEVEL);
  }
  
  public void HandleClose() {
    AppController.Instance.Quit();
  }

  public void HandleBack() {
    AppController.Instance.MenuStateController.TransitState(MenuStateController.Command.BACK);
  }
  
  public void GoToMainPanel() {
    ChooseLevelPanel.SetActive(false);
    ChooseShipPanel.SetActive(false);    
    MainPanel.SetActive(true);
  }
  
  public void GoToChooseShipPanel() {
    ChooseLevelPanel.SetActive(false);
    ChooseShipPanel.SetActive(true);    
    MainPanel.SetActive(false);
    SetPlayerShipVariation(DataController.Instance.PlayerShipVariationIndex);
  }
  
  public void GoToChooseLevelPanel() {
    ChooseLevelPanel.SetActive(true);
    ChooseShipPanel.SetActive(false);    
    MainPanel.SetActive(false);
    SetLevel(DataController.Instance.CurrentLevelIndex);
  }

  public void SetTotalSceore() {
    DataController.Instance.SetTotalScore();
    TotalScore.text = "Total score: " + DataController.Instance.TotalScore;
  }

  private void CofigureChooseShipPanelButtons() {
    chooseShipPanelItems = new Button[DataController.Instance.PlayerShipVariations.Length];
    for (int i = 0; i < DataController.Instance.PlayerShipVariations.Length; i++) {
      var item = Instantiate(MenuItemPrefab, ChooseShipPanelContent);
      item.GetComponentInChildren<Text>().text =
        //DataController.Instance.PlayerTypeOfStats + 
        " " + DataController.Instance.PlayerShipVariations[i].Id;
      int index = i;
      item.GetComponent<Button>().onClick.AddListener(() => {
        DataController.Instance.PlayerShipVariationIndex = index;
        DataController.Instance.SavePlayerShip();
        SetPlayerShipVariation(index);
      });
      
      chooseShipPanelItems[i] = item.GetComponent<Button>();
    }
  }

  private void SetPlayerShipVariation(int index) {
    if (chooseShipPanelItems == null)
      return;
    chooseShipPanelItems[index].Select();
    chooseShipPanelItems[index].OnSelect(null);

    if (PlayerShipVariationTransform.childCount > 0) {
      Destroy(PlayerShipVariationTransform.GetChild(0).gameObject);
    }
    Instantiate(DataController.Instance.PlayerShipVariations[index].BodyPrefab, PlayerShipVariationTransform);
  }
  
  private void CofigureChooseLevelPanelButtons() {
    chooseLevelPanelItems = new Button[DataController.Instance.LevelsConfig.Levels.Count];
    for (int i = 0; i < DataController.Instance.LevelsConfig.Levels.Count; i++) {
      var item = Instantiate(MenuItemPrefab, ChooseLevelPanelContent);
      item.GetComponentInChildren<Text>().text =
        DataController.Instance.LevelsConfig.Levels[i].Name;
      int index = i;
      item.GetComponent<Button>().onClick.AddListener(() => {
        DataController.Instance.CurrentLevelIndex = index;
        DataController.Instance.SaveCurrentLevel();
        SetLevel(index);
      });
      
      chooseLevelPanelItems[i] = item.GetComponent<Button>();
    }
  }
  
  private void SetLevel(int index) {
    if (chooseLevelPanelItems == null)
      return;
    chooseLevelPanelItems[index].Select();
    chooseLevelPanelItems[index].OnSelect(null);

    if (Background != null) {
      Destroy(Background);
    }
    Background = Instantiate(DataController.Instance.LevelsConfig.Levels[index].Background);
  }
}