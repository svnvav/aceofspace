﻿using UnityEngine;

public class DataController : MonoBehaviour
{
  public static DataController Instance;

  public ShipsConfig ShipsConfig;
  public LevelsConfig LevelsConfig;
  public BonusesConfig BonusesConfig;
  
  //crude implementation of weapons characteristics
  public Weapon PlayerMissile;
  public Weapon EnemyMissile;

  public int TotalScore;

  public string PlayerTypeOfStats = "Player";
  public int PlayerShipVariationIndex;

  public int CurrentLevelIndex;

  private ShipVariation[] playerShipVariations;
  public ShipVariation[] PlayerShipVariations
  {
    get { return playerShipVariations; }
  }

  private void Awake() {
    if (!Instance) Instance = this;
    
    if (ShipsConfig == null) {
      Debug.Log("Try to load ShipsStats.asset");
      ShipsConfig = Resources.Load<ShipsConfig>("ShipsStats");
    }

    playerShipVariations = GetShipVariationsOfType(PlayerTypeOfStats);
    
    if (LevelsConfig == null) {
      Debug.Log("Try to load LevelsConfig.asset");
      LevelsConfig = Resources.Load<LevelsConfig>("LevelsConfig");
    }
    
    if (BonusesConfig == null) {
      Debug.Log("Try to load BonusesConfig.asset");
      BonusesConfig = Resources.Load<BonusesConfig>("BonusesConfig");
    }

    if (PlayerPrefs.HasKey("Level"))
      CurrentLevelIndex = PlayerPrefs.GetInt("Level");
    
    if (PlayerPrefs.HasKey("PlayerShip"))
      PlayerShipVariationIndex = PlayerPrefs.GetInt("PlayerShip");
    
    DontDestroyOnLoad(this);
  }

  /// <summary>
  /// Customized for Player to simplify acess from MainMenuController and Player scripts
  /// </summary>
  public ShipStats GetPlayerStats() {
    int typeId = ShipsConfig.BaseStats.FindIndex(type => type.Type == PlayerTypeOfStats);
    if (typeId < 0) return null; //TODO: Handle exception "Can't find any configuration of the player ship"
    return GetUnitStats(PlayerShipVariationIndex);
  }

  public ShipStats GetUnitStats(int variationIndex) {
    var variation = ShipsConfig.ShipsVariations[variationIndex];
    var baseStats = ShipsConfig.BaseStats[variation.TypeId];

    return new ShipStats {
      MaxHealth = Mathf.RoundToInt(baseStats.Health * variation.HealthMultiplier),
      CurrentHealth = Mathf.RoundToInt(baseStats.Health * variation.HealthMultiplier),
      Mobility = Mathf.RoundToInt(baseStats.Mobility * variation.MobilityMultiplier),
      MoveSpeed = Mathf.RoundToInt(baseStats.MoveSpeed * variation.MoveSpeedMultiplier),
      ShootSpeed = baseStats.ShootSpeed / variation.ShootSpeedMultiplier,
      WeaponDamageMultiplier = variation.WeaponDamageMultiplier
    }; 
  }

  private ShipVariation[] GetShipVariationsOfType(string typeName) {
    
    int typeId = ShipsConfig.BaseStats.FindIndex(type => type.Type == typeName);
    
    if (typeId < 0) return new ShipVariation[0];
    return ShipsConfig.ShipsVariations.FindAll(e => e.TypeId == typeId).ToArray();
  }

  public void SaveCurrentLevel() {
    PlayerPrefs.SetInt("Level", CurrentLevelIndex);
    PlayerPrefs.Save();
  }
  
  public void SavePlayerShip() {
    PlayerPrefs.SetInt("PlayerShip", PlayerShipVariationIndex);
    PlayerPrefs.Save();
  }

  public void SetTotalScore() {
    TotalScore = 0;
    for (int i = 0; i < LevelsConfig.Levels.Count; i++) {
      if (PlayerPrefs.HasKey("Level" + i))
        TotalScore += PlayerPrefs.GetInt("Level" + i);
    }
  }
  
  public void SaveLevelScore(int levelIndex, int score) {
    if (!PlayerPrefs.HasKey("Level" + levelIndex) || PlayerPrefs.GetInt("Level" + levelIndex) < score) {
      PlayerPrefs.SetInt("Level" + levelIndex, score);
      PlayerPrefs.Save();
      SetTotalScore();
    }
  }
  
  public int GetLevelScore(int index) {
    return PlayerPrefs.HasKey("Level" + index) ? PlayerPrefs.GetInt("Level" + index) : 0;
  }
 
}