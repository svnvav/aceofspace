﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BonusesController : MonoBehaviour
{
  public static BonusesController Instance;

  private Coroutine shootSpeedCoroutine;
  private Coroutine damageCoroutine;
  private Coroutine shieldCoroutine;
  private Coroutine freezeCoroutine;

  private void Awake() {
    if (!Instance) Instance = this;
  }

  public void GiveHealth(Ship ship, int value) {
    ship.Stats.CurrentHealth += value;
    ship.Stats.CurrentHealth = Mathf.Clamp(ship.Stats.CurrentHealth, 0, ship.Stats.MaxHealth);
    if (ship.gameObject.CompareTag("Player"))
      GameHUDController.Instance.SetHitPointsValues(
        GameController.Instance.Player.Stats.MaxHealth,
        GameController.Instance.Player.Stats.CurrentHealth);
  }

  public void StopAll() {
    if (shootSpeedCoroutine != null) {
      GameHUDController.Instance.ShootSpeedBonusTimer.gameObject.SetActive(false);
      StopCoroutine(shootSpeedCoroutine);
    }
    
    if (damageCoroutine != null) {
      GameHUDController.Instance.DamageBonusTimer.gameObject.SetActive(false);
      StopCoroutine(damageCoroutine);
    }
    
    if (shieldCoroutine != null) {
      GameHUDController.Instance.ShieldBonusTimer.gameObject.SetActive(false);
      StopCoroutine(shieldCoroutine);
    }

    foreach (var bonus in GameObject.FindGameObjectsWithTag("Bonus")) {
      Destroy(bonus);
    }
  }
  
  public void MultiplyShootSpeed(Ship ship, float multiplier, float time) {
    if (shootSpeedCoroutine != null) {
      StopCoroutine(shootSpeedCoroutine);
      ship.Stats.ShootSpeed *= multiplier;
    }
    ship.Stats.ShootSpeed /= multiplier;

    GameHUDController.Instance.ShootSpeedBonusTimer.gameObject.SetActive(true);
    GameHUDController.Instance.ShootSpeedBonusTimer.value = 1;
    GameHUDController.Instance.ShootSpeedBonusTimer.GetComponentInChildren<Text>().text = "Shoot speed x" + multiplier;
    
    shootSpeedCoroutine = StartCoroutine(ShootSpeedCoroutine(ship, multiplier, time));
  }

  IEnumerator ShootSpeedCoroutine(Ship ship, float multiplier, float time) {
    float timer = 0.0f;
    while (timer < time) {
      GameHUDController.Instance.ShootSpeedBonusTimer.value = 1 - timer/time;
      timer += Time.deltaTime;
      yield return null;
    }
    
    ship.Stats.ShootSpeed *= multiplier;
    GameHUDController.Instance.ShootSpeedBonusTimer.gameObject.SetActive(false);
    shootSpeedCoroutine = null;
  }
  
  public void MultiplyDamage(Ship ship, float multiplier, float time) {
    if (damageCoroutine != null) {
      StopCoroutine(damageCoroutine);
      ship.Stats.WeaponDamageMultiplier /= multiplier;
    }
    ship.Stats.WeaponDamageMultiplier *= multiplier;

    GameHUDController.Instance.DamageBonusTimer.gameObject.SetActive(true);
    GameHUDController.Instance.DamageBonusTimer.value = 1;
    GameHUDController.Instance.DamageBonusTimer.GetComponentInChildren<Text>().text = "Damage x" + multiplier;
    
    damageCoroutine = StartCoroutine(DamageCoroutine(ship, multiplier, time));
  }

  IEnumerator DamageCoroutine(Ship ship, float multiplier, float time) {
    float timer = 0.0f;
    while (timer < time) {
      GameHUDController.Instance.DamageBonusTimer.value = 1 - timer/time;
      timer += Time.deltaTime;
      yield return null;
    }
    
    ship.Stats.WeaponDamageMultiplier /= multiplier;
    GameHUDController.Instance.DamageBonusTimer.gameObject.SetActive(false);
    damageCoroutine = null;
  }
  
  public void GiveShield(Ship ship, float time) {
    if (shieldCoroutine != null) {
      StopCoroutine(shieldCoroutine);
    }

    GameHUDController.Instance.ShieldBonusTimer.gameObject.SetActive(true);
    GameHUDController.Instance.ShieldBonusTimer.value = 1;
    
    shieldCoroutine = StartCoroutine(ShieldCoroutine(ship, time));
  }

  IEnumerator ShieldCoroutine(Ship ship, float time) {
    float timer = 0.0f;
    int health = ship.Stats.CurrentHealth;
    while (timer < time) {
      if (ship.Stats.CurrentHealth < health)
        ship.Stats.CurrentHealth = health;
      else {
        health = ship.Stats.CurrentHealth;
      }
      GameHUDController.Instance.ShieldBonusTimer.value = 1 - timer/time;
      timer += Time.deltaTime;
      yield return null;
    }
    
    GameHUDController.Instance.ShieldBonusTimer.gameObject.SetActive(false);
    shieldCoroutine = null;
  }
  
  public void Freeze(Ship ship, float time) {
    if (freezeCoroutine != null) {
      StopCoroutine(freezeCoroutine);
    }
    
    freezeCoroutine = StartCoroutine(FreezeCoroutine(ship, time, ship.Stats.MoveSpeed));
  }

  IEnumerator FreezeCoroutine(Ship ship, float time, int moveSpeed) {
    ship.Stats.MoveSpeed = 0;
    yield return new WaitForSeconds(time);
    ship.Stats.MoveSpeed = moveSpeed;
    freezeCoroutine = null;
  }
}