﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelsConfig))]
public class LevelsConfigEditor : Editor
{
  public override void OnInspectorGUI() {

    EditorGUILayout.HelpBox(
      "To edit levels config use special window.",
      MessageType.Info);


    if (GUILayout.Button("Open manager window")) {
      EditorWindow.GetWindow(typeof(LevelsManager));
    }
  }
}