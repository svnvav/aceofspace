﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Ship), true)]
public class ShipEditor : Editor
{
  public ShipsConfig Config;
  
  private void OnEnable() {
    if (Config == null) {
      Config = AssetDatabase.LoadAssetAtPath<ShipsConfig>("Assets/Resources/ShipsStats.asset");
    }
  }

  public override void OnInspectorGUI() {
    Ship ship = (Ship) target;

    string[] types = new string[Config.ShipsVariations.Count];

    for (int i = 0; i < types.Length; i++) {
      types[i] = Config.BaseStats[Config.ShipsVariations[i].TypeId].Type + " " + Config.ShipsVariations[i].Id;
    }

    int old = ship.ConfigurationIndex;
    ship.ConfigurationIndex = EditorGUILayout.Popup("Type", ship.ConfigurationIndex, types);

    if (old != ship.ConfigurationIndex) {
      if (ship.BodyTransform.childCount > 0) {
        DestroyImmediate(ship.BodyTransform.GetChild(0).gameObject);
      }
      Instantiate(Config.ShipsVariations[ship.ConfigurationIndex].BodyPrefab, ship.BodyTransform);

      /*var variation = Config.ShipsVariations[ship.ConfigurationIndex];
      var baseStats = Config.BaseStats[variation.TypeId];
      
      ship.Stats = new ShipStats {
        MaxHealth = baseStats.Health * variation.HealthMultiplier,
        CurrentHealth = baseStats.Health * variation.HealthMultiplier,
        Mobility = baseStats.Mobility * variation.MobilityMultiplier,
        MoveSpeed = baseStats.MoveSpeed * variation.MoveSpeedMultiplier,
        ShootSpeed = (float)baseStats.ShootSpeed / variation.ShootSpeedMultiplier,
        WeaponDamageMultiplier = variation.WeaponDamageMultiplier
      };*/
    }
    
    DrawDefaultInspector();

  }
}
