﻿using UnityEditor;
using UnityEngine;

public class CustomStyles
{
  public static readonly GUIStyle FoldoutHeader =
    new GUIStyle(EditorStyles.miniButton) {
      padding = new RectOffset(10, 3, 0, 0),
      alignment = TextAnchor.MiddleCenter,
      margin = new RectOffset(5, 5, 0, 0)
    };
  
  public static readonly GUIStyle HeaderButton =
    new GUIStyle(EditorStyles.miniButtonMid) {
      padding = new RectOffset(0, 0, 0, 0),
      margin = new RectOffset(0, 0, 0, 0),
      alignment = TextAnchor.MiddleCenter,
      fixedHeight = 18
    };
  
  public static readonly GUIStyle StretchedButton =
    new GUIStyle(GUI.skin.button) {
      alignment = TextAnchor.MiddleCenter,
      stretchWidth = true
    };
  
  public static readonly GUIStyle CenteredLabel =
    new GUIStyle(EditorStyles.helpBox) {
      alignment = TextAnchor.MiddleCenter,
      fontSize = 12
    };

  public static readonly GUIStyle ListButton =
    new GUIStyle(EditorStyles.miniButton) {
      alignment = TextAnchor.MiddleCenter,
      padding = new RectOffset(0, 0, 0, 0),
      margin = new RectOffset(5, 5, 0, 0),
      fixedHeight = 18
    };
}