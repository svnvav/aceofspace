﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ShipsStatsManager : EditorWindow
{
  public ShipsConfig Config;
  public List<bool> BaseStatsFoldouts;
  public List<bool> ShipsStatsFoldouts;

  private Vector2 scrollPos;

  [MenuItem("Tools/Ships Stats Manager")]
  public static void ShowWindow() {
    GetWindow(typeof(ShipsStatsManager));
  }

  private void OnEnable() {
    if (Config == null) {
      Config = AssetDatabase.LoadAssetAtPath<ShipsConfig>("Assets/Resources/ShipsStats.asset");
    }

    if (Config == null) {
      Debug.Log("Ships config was created");
      Config = CreateInstance<ShipsConfig>();
      AssetDatabase.CreateAsset(Config, "Assets/Resources/ShipsStats.asset");
    }

    if (Config.BaseStats == null) {
      Debug.Log("Ships config BaseStats was created");
      Config.BaseStats = new List<BaseShipStats>();
    }

    BaseStatsFoldouts = new List<bool>();
    for (int i = 0; i < Config.BaseStats.Count; i++) {
      BaseStatsFoldouts.Add(false);
    }

    if (Config.ShipsVariations == null) {
      Debug.Log("Ships config ShipsVariations was created");
      Config.ShipsVariations = new List<ShipVariation>();
    }

    ShipsStatsFoldouts = new List<bool>();
    for (int i = 0; i < Config.ShipsVariations.Count; i++) {
      ShipsStatsFoldouts.Add(false);
    }
  }

  void OnGUI() {
    if(EditorGUIUtility.isProSkin)
      CustomStyles.CenteredLabel.normal.textColor = Color.white;
    
    if (Config == null) {
      Config = Resources.FindObjectsOfTypeAll<ShipsConfig>()[0];
    }

    Config = (ShipsConfig) EditorGUILayout.ObjectField(Config, typeof(ScriptableObject), false);

    scrollPos = GUILayout.BeginScrollView(scrollPos, false, false);

    
    GUILayout.Label("Base Ships Stats", CustomStyles.CenteredLabel);


    if (GUILayout.Button("Add", CustomStyles.StretchedButton)) {
      Config.BaseStats.Add(new BaseShipStats());
      BaseStatsFoldouts.Add(false);
    }

    for (int i = 0; i < Config.BaseStats.Count; i++) {
      DrawBaseType(i);
    }

    GUILayout.Label("Ships Stats Multipliers", CustomStyles.CenteredLabel);

    if (GUILayout.Button("Add", CustomStyles.StretchedButton)) {
      Config.ShipsVariations.Add(new ShipVariation());
      ShipsStatsFoldouts.Add(false);
    }

    for (int i = 0; i < Config.ShipsVariations.Count; i++) {
      DrawShipMultipliers(i);
    }

    EditorGUILayout.Space();

    GUILayout.EndScrollView();
    
    EditorUtility.SetDirty(Config);
  }

  void DrawBaseType(int index) {
    var element = Config.BaseStats[index];

    GUILayout.BeginHorizontal(CustomStyles.FoldoutHeader);
    BaseStatsFoldouts[index] =
      EditorGUILayout.Foldout(BaseStatsFoldouts[index], element.Type, true, EditorStyles.foldout);
    if (GUILayout.Button("Delete", CustomStyles.HeaderButton, GUILayout.Width(50))) {
      int i = 0;
      while (i < Config.ShipsVariations.Count) {
        if (Config.ShipsVariations[i].TypeId == index) {
          Config.ShipsVariations.RemoveAt(i);
          ShipsStatsFoldouts.RemoveAt(i);
        }
        else {
          i++;
        }
      }

      Config.BaseStats.RemoveAt(index);
      BaseStatsFoldouts.RemoveAt(index);
      return;
    }
    
    if(!EditorGUIUtility.isProSkin) GUILayout.Label("", CustomStyles.HeaderButton, GUILayout.Width(7)); //just for the vertical separator

    GUILayout.EndHorizontal();

    if (BaseStatsFoldouts[index]) {
      element.Type = EditorGUILayout.TextField("Type", element.Type);
      element.Health = EditorGUILayout.IntField("Health (unit)", element.Health);
      element.ShootSpeed = EditorGUILayout.IntField("Shoot speed (shoot/ms)", element.ShootSpeed);
      element.MoveSpeed = EditorGUILayout.IntField("Move speed (unit)", element.MoveSpeed);
      element.Mobility = EditorGUILayout.IntField("Mobility (%)", element.Mobility);
    }
  }

  void DrawShipMultipliers(int index) {
    var element = Config.ShipsVariations[index];

    GUILayout.BeginHorizontal(CustomStyles.FoldoutHeader);

    ShipsStatsFoldouts[index] = EditorGUILayout.Foldout(ShipsStatsFoldouts[index],
      Config.BaseStats[element.TypeId].Type + " " + element.Id, true);

    if (GUILayout.Button("Delete", CustomStyles.HeaderButton, GUILayout.Width(50))) {
      Config.ShipsVariations.RemoveAt(index);
      ShipsStatsFoldouts.RemoveAt(index);
      return;
    }
    
    if(!EditorGUIUtility.isProSkin) GUILayout.Label("", CustomStyles.HeaderButton, GUILayout.Width(7)); //just for the vertical separator

    GUILayout.EndHorizontal();

    if (ShipsStatsFoldouts[index]) {
      string[] types = new string[Config.BaseStats.Count];

      for (int i = 0; i < types.Length; i++) {
        types[i] = Config.BaseStats[i].Type;
      }

      element.TypeId = EditorGUILayout.Popup("Type", element.TypeId, types);
      element.Id = EditorGUILayout.TextField("Identifier", element.Id);
      element.BodyPrefab = 
        (GameObject) EditorGUILayout.ObjectField("Body Prefab", element.BodyPrefab, typeof(GameObject), false);
      GUILayout.Label("Multipliers: ");
      element.HealthMultiplier = EditorGUILayout.FloatField("Health", element.HealthMultiplier);
      element.ShootSpeedMultiplier = EditorGUILayout.FloatField("Shoot speed", element.ShootSpeedMultiplier);
      element.MoveSpeedMultiplier = EditorGUILayout.FloatField("Move speed", element.MoveSpeedMultiplier);
      element.MobilityMultiplier = EditorGUILayout.FloatField("Mobility", element.MobilityMultiplier);
      element.WeaponDamageMultiplier = EditorGUILayout.FloatField("Weapon damage", element.WeaponDamageMultiplier);
    }
  }
}