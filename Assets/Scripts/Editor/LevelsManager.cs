﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class LevelsManager : EditorWindow
{
  public LevelsConfig Config;
  public List<bool> Foldouts;

  private Vector2 scrollPos;
  
  [MenuItem("Tools/Levels Manager")]
  public static void ShowWindow() {
    GetWindow(typeof(LevelsManager));
  }

  private void OnEnable() {
    if (Config == null) {
      Config = AssetDatabase.LoadAssetAtPath<LevelsConfig>("Assets/Resources/LevelsConfig.asset");
    }

    if (Config == null) {
      Debug.Log("Levels config was created");
      Config = CreateInstance<LevelsConfig>();
      AssetDatabase.CreateAsset(Config, "Assets/Resources/LevelsConfig.asset");
    }

    if (Config.Levels == null) {
      Debug.Log("Levels config collection was created");
      Config.Levels = new List<LevelSettings>();
    }

    Foldouts = new List<bool>();
    for (int i = 0; i < Config.Levels.Count; i++) {
      Foldouts.Add(false);
    }
  }
  
  

  void OnGUI() {
    
    if (Config == null) {
      Config = Resources.FindObjectsOfTypeAll<LevelsConfig>()[0];
    }

    Config = (LevelsConfig) EditorGUILayout.ObjectField(Config, typeof(ScriptableObject), false);

    scrollPos = GUILayout.BeginScrollView(scrollPos, false, false);

    if (GUILayout.Button("Add", CustomStyles.StretchedButton)) {
      Config.Levels.Add(new LevelSettings());
      Foldouts.Add(false);
    }

    for (int i = 0; i < Config.Levels.Count; i++) {
      DrawLevelFoldout(i);
    }

    EditorGUILayout.Space();

    GUILayout.EndScrollView();
    
    EditorUtility.SetDirty(Config);
  }

  void DrawLevelFoldout(int index) {
    var element = Config.Levels[index];

    
    GUILayout.BeginHorizontal(CustomStyles.FoldoutHeader);

    Foldouts[index] = EditorGUILayout.Foldout(Foldouts[index],
      Config.Levels[index].Name, true);

    if (GUILayout.Button("Delete", CustomStyles.HeaderButton, GUILayout.Width(50))) {
      Config.Levels.RemoveAt(index);
      Foldouts.RemoveAt(index);
      return;
    }


    GUI.enabled = index > 0;
    if (GUILayout.Button("▲", CustomStyles.HeaderButton, GUILayout.Width(20))) {
      Config.Levels[index] = Config.Levels[index - 1];
      Config.Levels[index - 1] = element;
      bool temp = Foldouts[index];
      Foldouts[index] = Foldouts[index - 1];
      Foldouts[index - 1] = temp;
      return;
    }
    GUI.enabled = true;
    
    GUI.enabled = index < Config.Levels.Count - 1;
    if (GUILayout.Button("▼", CustomStyles.HeaderButton, GUILayout.Width(20))) {
      Config.Levels[index] = Config.Levels[index + 1];
      Config.Levels[index + 1] = element;
      bool temp = Foldouts[index];
      Foldouts[index] = Foldouts[index + 1];
      Foldouts[index + 1] = temp;
      return;
    }
    GUI.enabled = true;

    if(!EditorGUIUtility.isProSkin) GUILayout.Label("", CustomStyles.HeaderButton, GUILayout.Width(7)); //just for the vertical separator

    GUILayout.EndHorizontal();

    if (Foldouts[index]) {
      
      element.Name = EditorGUILayout.TextField("Name", element.Name);
      element.Background = 
        (GameObject) EditorGUILayout.ObjectField("Background Prefab", element.Background, typeof(GameObject), false);
      element.AudioClip = 
        (AudioClip) EditorGUILayout.ObjectField("Audio Clip", element.AudioClip, typeof(AudioClip), false);
      GUILayout.BeginHorizontal();
      GUILayout.Label("Enemies waves:");
      if (GUILayout.Button("Add", CustomStyles.ListButton, GUILayout.Width(50))) {
        element.Waves.Add(null);
        return;
      }
      GUILayout.EndHorizontal();
      for (int i = 0; i < element.Waves.Count; i++) {
        GUILayout.BeginHorizontal();
        GUILayout.Label("" + i, GUILayout.Width(30));
        element.Waves[i] = (SquadConfig) EditorGUILayout.ObjectField(element.Waves[i], typeof(ScriptableObject), false);
        if (GUILayout.Button("Delete", CustomStyles.ListButton, GUILayout.Width(50))) {
          element.Waves.RemoveAt(i);
          return;
        }
        GUILayout.EndHorizontal();
      }
    }
  }

  
}