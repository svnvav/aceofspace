﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SquadConfigGameObject))]
public class SquadConfigEditor : Editor
{
  private string fileName = "Squad";

  public override void OnInspectorGUI() {
    SquadConfigGameObject gameObject = (SquadConfigGameObject) target;

    EditorGUILayout.HelpBox(
      "Place ships as a childs of that object, than type a file name for saving and ckick 'Save...' button",
      MessageType.Info);

    fileName = EditorGUILayout.TextField("File name", fileName);

    if (GUILayout.Button("Save to /Assets/Resources/Squads/" + fileName + ".asset")) {
      SquadConfig squadConfig = CreateInstance<SquadConfig>();
      squadConfig.Positions = new SquadPosition[gameObject.transform.childCount];
      for (int i = 0; i < gameObject.transform.childCount; i++) {
        var child = gameObject.transform.GetChild(i);
        squadConfig.Positions[i] = new SquadPosition {
          Position = child.localPosition,
          UnitVariationId = child.gameObject.GetComponent<Ship>().ConfigurationIndex
        };
      }

      if (!AssetDatabase.IsValidFolder("Assets/Resources/Squads"))
        AssetDatabase.CreateFolder("Assets/Resources", "Squads");
      AssetDatabase.CreateAsset(squadConfig, "Assets/Resources/Squads/" + fileName + ".asset");
    }
  }
}