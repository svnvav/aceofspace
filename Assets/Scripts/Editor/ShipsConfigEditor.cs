﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ShipsConfig))]
public class ShipsConfigEditor : Editor
{
  public override void OnInspectorGUI() {

    EditorGUILayout.HelpBox(
      "To edit ships stats use special window.",
      MessageType.Info);


    if (GUILayout.Button("Open manager window")) {
      EditorWindow.GetWindow(typeof(ShipsStatsManager));
    }
  }
}