﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BonusesConfig))]
public class BonusesConfigEditor : Editor
{
  public override void OnInspectorGUI() {

    EditorGUILayout.HelpBox(
      "To edit bonuses config use special window.",
      MessageType.Info);


    if (GUILayout.Button("Open manager window")) {
      EditorWindow.GetWindow(typeof(BonusesManager));
    }
  }
}