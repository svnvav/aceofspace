﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class BonusesManager : EditorWindow
{
  public BonusesConfig Config;
  public List<bool> Foldouts;
  
  private Vector2 scrollPos;

  [MenuItem("Tools/Bonuses Manager")]
  public static void ShowWindow() {
    GetWindow(typeof(BonusesManager));
  }

  private void OnEnable() {
    if (Config == null) {
      Config = AssetDatabase.LoadAssetAtPath<BonusesConfig>("Assets/Resources/BonusesConfig.asset");
    }

    if (Config == null) {
      Debug.Log("BonusesConfig was created");
      Config = CreateInstance<BonusesConfig>();
      AssetDatabase.CreateAsset(Config, "Assets/Resources/BonusesConfig.asset");
    }

    if (Config.Bonuses == null) {
      Debug.Log("BonusesConfig BaseStats was created");
      Config.Bonuses = new List<BonusSettings>();
    }

    Foldouts = new List<bool>();
    for (int i = 0; i < Config.Bonuses.Count; i++) {
      Foldouts.Add(false);
    }
  }

  void OnGUI() {
    
    if (Config == null) {
      Config = Resources.FindObjectsOfTypeAll<BonusesConfig>()[0];
    }

    Config = (BonusesConfig) EditorGUILayout.ObjectField(Config, typeof(ScriptableObject), false);

    scrollPos = GUILayout.BeginScrollView(scrollPos, false, false);



    if (GUILayout.Button("Add", CustomStyles.StretchedButton)) {
      Config.Bonuses.Add(new BonusSettings());
      Foldouts.Add(false);
    }

    for (int i = 0; i < Config.Bonuses.Count; i++) {
      DrawBonusInspector(i);
    }

    EditorGUILayout.Space();

    GUILayout.EndScrollView();
    
    EditorUtility.SetDirty(Config);
  }

  private void DrawBonusInspector(int index) {
    var element = Config.Bonuses[index];

    GUILayout.BeginHorizontal(CustomStyles.FoldoutHeader);
    
    Foldouts[index] =
      EditorGUILayout.Foldout(Foldouts[index], element.Name, true, EditorStyles.foldout);
    if (GUILayout.Button("Delete", CustomStyles.HeaderButton, GUILayout.Width(50))) {
      Config.Bonuses.RemoveAt(index);
      Foldouts.RemoveAt(index);
      return;
    }
    
    if(!EditorGUIUtility.isProSkin) GUILayout.Label("", CustomStyles.HeaderButton, GUILayout.Width(7)); //just for the vertical separator

    GUILayout.EndHorizontal();

    if (Foldouts[index]) {
      element.Name = EditorGUILayout.TextField("Name", element.Name);
      element.Prefab = (GameObject) EditorGUILayout.ObjectField("Script", element.Prefab, typeof(GameObject), false);
    }
  }

}