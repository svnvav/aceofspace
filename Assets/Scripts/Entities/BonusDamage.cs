﻿using UnityEngine;

public class BonusDamage : Bonus
{
  public float Multiplier; //shouldn't bu zero
  public float Time;
  
  private void OnTriggerEnter2D(Collider2D other) {
    if (other.CompareTag("Player")) {
      BonusesController.Instance.MultiplyDamage(other.GetComponent<Ship>(), Multiplier, Time);
      GameController.Instance.AudioSource.PlayOneShot(OnTake);
      Destroy(gameObject);
    }
  }
}