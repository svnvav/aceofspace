﻿using UnityEngine;


[System.Serializable]
public class Weapon
{
  public int Damage;
  public int MoveSpeed;
  public Sprite Sprite;
}