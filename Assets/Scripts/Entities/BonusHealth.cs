﻿using UnityEngine;

public class BonusHealth : Bonus
{
  public int HealthBonus;
  
  private void OnTriggerEnter2D(Collider2D other) {
    if (other.CompareTag("Player")) {
      BonusesController.Instance.GiveHealth(other.GetComponent<Ship>(), HealthBonus);
      GameController.Instance.AudioSource.PlayOneShot(OnTake);
      Destroy(gameObject);
    }
  }
}