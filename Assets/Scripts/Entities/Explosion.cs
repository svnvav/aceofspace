﻿using System.Collections;
using UnityEngine;

public class Explosion : MonoBehaviour
{
  void OnEnable() {
    StartCoroutine(PushToPool());
  }

  IEnumerator PushToPool() {
    yield return new WaitForSeconds(2);
    ExplosionsPool.Instance.Push(gameObject);
  }
}