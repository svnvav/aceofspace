﻿using System.Collections;
using JetBrains.Annotations;
using UnityEngine;

[System.Serializable]
public class Boundary
{
	public float MinX, MaxX, MinY, MaxY;
}

public class Player : Ship
{
	public Boundary Boundary; //Game area
	
	private float reloadTimer = -0.0f;

	void Start() {
		Stats = DataController.Instance.GetPlayerStats();
	}

	void Update () {
		Vector3 cursorInWorldPos = Camera.main.ScreenToWorldPoint( Input.mousePosition );

		//player should not reach the cursor point because at that point a movement mechancis is inconvenient
		if (((Vector2)cursorInWorldPos - (Vector2)transform.position).magnitude > 2) {
			float verticalDirection = Input.GetAxisRaw("Vertical");
			Move(transform.up * verticalDirection);
		}
		
		float horizontalDirection = Input.GetAxisRaw("Horizontal");
		Move(transform.right * horizontalDirection);

		RotateTo(cursorInWorldPos);
		
		if (Input.GetMouseButton(0) && reloadTimer <= 0.0f) {
			SpawnMissiles("PlayerShot");   
			reloadTimer += Stats.ShootSpeed/1000;
		}
		if(reloadTimer > 0.0f) {
			reloadTimer -= Time.deltaTime;
		}
	}

	public override void Move(Vector3 movingDirection) {
		base.Move(movingDirection);
		if (transform.position.x < Boundary.MinX || transform.position.x > Boundary.MaxX ||
		    transform.position.y < Boundary.MinY || transform.position.y > Boundary.MaxY)
			transform.position = new Vector3(
				Mathf.Clamp(transform.position.x, Boundary.MinX, Boundary.MaxX),
				Mathf.Clamp(transform.position.y, Boundary.MinY, Boundary.MaxY),
				0.0f
			);
	}

	public override void Die() {
		
		base.Die();

		BodyTransform.gameObject.SetActive(false);
		StartCoroutine(DefeatAfterDelay());
	}

	IEnumerator DefeatAfterDelay() {
		yield return new WaitForSecondsRealtime(0.25f);
		AppController.Instance.GameStateController.TransitState(GameStateController.Command.DEFEAT);
	}

	/// <param name="variationIndex">PlayerShipVariationIndex at DataController, not in ShipsStatsCongig</param>
	public override void SetBody(int variationIndex) {

		if (BodyTransform.childCount > 0) {
			Destroy(BodyTransform.GetChild(0).gameObject);
		}
		Instantiate(DataController.Instance.PlayerShipVariations[variationIndex].BodyPrefab, BodyTransform);
	}
}
