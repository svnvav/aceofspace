﻿using System.Security.Cryptography;
using UnityEngine;

public class Shot : MonoBehaviour
{
  public float Speed;
  public int Damage;

  private void OnTriggerExit2D(Collider2D other) {
    if (other.gameObject.CompareTag("GameArea")) {
      ShotsPool.Instance.Push(gameObject);
      gameObject.SetActive(false);
    }
  }
  
  void OnTriggerEnter2D(Collider2D other) {
    
    if (gameObject.CompareTag("PlayerShot") && other.gameObject.CompareTag("Enemy") ||
        gameObject.CompareTag("EnemyShot") && other.gameObject.CompareTag("Player")) {

      if (other.gameObject.GetComponent<Ship>().GetDamage(Damage)) {
        Die();
      }
      return;
    }

    //when shots destroy each other
    if (gameObject.CompareTag("PlayerShot") && other.gameObject.CompareTag("EnemyShot") || 
        gameObject.CompareTag("EnemyShot") && other.gameObject.CompareTag("PlayerShot")) {
      Die();
    }
  }

  void Die() {
    gameObject.SetActive(false);
    ShotsPool.Instance.Push(gameObject);
      
    var explosion = ExplosionsPool.Instance.Pop();
    explosion.transform.position = transform.position;
    explosion.transform.rotation = transform.rotation;
    explosion.SetActive(true);
    ExplosionsPool.Instance.PushAfterSeconds(explosion, 2);
  }
  
  void Update() {
    Move();
  }

  public virtual void Move() {
    transform.position += transform.up * Speed * Time.deltaTime;
  }
  
  
}