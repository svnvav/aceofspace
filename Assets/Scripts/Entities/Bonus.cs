﻿using UnityEngine;
using UnityEngine.Events;

public class Bonus : MonoBehaviour
{
  public AudioClip OnTake;
  
  private void Update() {
    transform.Translate(0, -1.5f * Time.deltaTime, 0);
  }
  
  private void OnTriggerExit2D(Collider2D other) {
    if (other.gameObject.CompareTag("GameArea")) {
      Destroy(gameObject);
    }
  }
}