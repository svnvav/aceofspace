﻿using UnityEngine;

public class EnemyAttentionArea : MonoBehaviour
{
  public Enemy Enemy;
  
  private void OnTriggerEnter2D(Collider2D other) {
    if (other.CompareTag("Player")) {
      Enemy.PlayerTransform = other.transform;
    }
  }
  
  private void OnTriggerExit2D(Collider2D other) {
    if (other.CompareTag("Player")) {
      Enemy.PlayerTransform = null;
    }
  }
  
}