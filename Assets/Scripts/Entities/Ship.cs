﻿using UnityEngine;
using UnityEngine.UI;

public class ShipStats
{
  public int MaxHealth;
  public int CurrentHealth;
  public float ShootSpeed;
  public int MoveSpeed;
  public int Mobility;
  public float WeaponDamageMultiplier;
}

public class Ship : MonoBehaviour
{
  public ShipStats Stats;
  [HideInInspector] public int ConfigurationIndex;
  public GameObject HealthBar;
  public Transform BodyTransform;
  public Transform[] MissilesSpawns;
  public GameObject ExplosionPrefab;

  public virtual void Move(Vector3 movingDirection) {
    transform.position += movingDirection.normalized * Stats.MoveSpeed * Time.deltaTime;
  }

  public virtual void RotateTo(Vector3 rotationDirection) {
    Vector3 diff = rotationDirection - transform.position;
    diff.Normalize();

    transform.rotation = Quaternion.Euler(0f, 0f, Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg - 90);
  }

  public virtual bool GetDamage(int value) {
    if (Random.Range(0, 100) < Stats.Mobility) {
      //Debug.Log("Miss: " + Stats.CurrentHealth);
      return false;
    }

    Stats.CurrentHealth -= value;
    //Debug.Log(gameObject.tag + " damaged: " + Stats.CurrentHealth);
    if(!HealthBar.activeSelf)
      HealthBar.SetActive(true);
    HealthBar.GetComponentInChildren<Slider>().value = (float)Stats.CurrentHealth / Stats.MaxHealth;
    if (Stats.CurrentHealth <= 0)
      Die();
    return true;
  }

  public virtual void Die() {
    gameObject.tag = "Dead";
    Destroy(Instantiate(ExplosionPrefab, transform.position, transform.rotation), 1);
  }

  public virtual void SpawnMissiles(string missileTag) {
    foreach (var spawn in MissilesSpawns) {
      var missile = ShotsPool.Instance.Pop();
      missile.tag = missileTag;
      missile.SetActive(true);
      Weapon weapon = gameObject.CompareTag("Player")
        ? DataController.Instance.PlayerMissile
        : DataController.Instance.EnemyMissile;
      missile.GetComponent<Shot>().Damage =
        Mathf.RoundToInt(weapon.Damage * Stats.WeaponDamageMultiplier);
      missile.GetComponent<Shot>().Speed = weapon.MoveSpeed;
      missile.GetComponentInChildren<SpriteRenderer>().sprite = weapon.Sprite;
      missile.transform.position = spawn.position;
      missile.transform.rotation = spawn.rotation;
      missile.transform.localScale = Vector3.one * 0.8f + Vector3.one * Stats.WeaponDamageMultiplier * 0.1f;
    }
  }

  public virtual void SetBody(int variationIndex) {
    if (BodyTransform.childCount > 0) {
      Destroy(BodyTransform.GetChild(0).gameObject);
    }

    Instantiate(DataController.Instance.ShipsConfig.ShipsVariations[variationIndex].BodyPrefab, BodyTransform);
  }
}