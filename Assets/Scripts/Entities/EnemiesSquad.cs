﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesSquad : MonoBehaviour
{
  public float Speed;
  
  public List<Enemy> Units;

  private void OnEnable() {
    foreach (var unit in Units)
      unit.Squad = this;
  }

  private void Update() {
    
    Move();
  }

  private void Move() {
    foreach (var unit in Units)
      if((float)unit.Stats.CurrentHealth / unit.Stats.MaxHealth >= unit.InSquadHealthBarrier)
        unit.transform.position += transform.up * Speed * Time.deltaTime;
  }

  /// <summary>
  /// Used for fluent but fast appearing of enemies in the game area
  /// </summary>
  /// <param name="distance">Distance between spawn and appearing position</param>
  /// <param name="time">Time to overcome distance</param>
  public void Warp(float distance, float time) {
    StartCoroutine(WarpRoutine(distance, time));
  }

  IEnumerator WarpRoutine(float distance, float time) {
    float timer = 0.0f;
    while (timer < time) {
      foreach (var unit in Units)
        if((float)unit.Stats.CurrentHealth / unit.Stats.MaxHealth >= unit.InSquadHealthBarrier)
          unit.transform.position += transform.up * distance * Time.deltaTime;
      timer += Time.deltaTime;
      yield return null;
    }
  }

  public void Clear() {
    while (Units.Count > 0) {
      DestroyImmediate(Units[Units.Count - 1].gameObject);
      Units.RemoveAt(Units.Count - 1);
    }
  }
  
}