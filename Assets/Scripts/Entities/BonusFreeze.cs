﻿using UnityEngine;

public class BonusFreeze : Bonus
{
  public float Time;
  
  private void OnTriggerEnter2D(Collider2D other) {
    if (other.CompareTag("Player")) {
      BonusesController.Instance.Freeze(other.GetComponent<Ship>(), Time);
      GameController.Instance.AudioSource.PlayOneShot(OnTake);
      Destroy(gameObject);
    }
  }
}