﻿using UnityEngine;
using UnityEngine.UI;

public class Enemy : Ship
{
  [Header("Enemy:")] public EnemiesSquad Squad;
  [Range(0, 1)] public float InSquadHealthBarrier;

  [HideInInspector] public Transform PlayerTransform;

  private Vector3 healthBarCanvasOffset;

  private float reloadTimer = -0.0f;

  private void Awake() {
    HealthBar.GetComponentInChildren<Slider>().value = 1;
    HealthBar.gameObject.SetActive(false);
    healthBarCanvasOffset =
      transform.position - HealthBar.transform.position; //needs to keep the health bar always above the enemy model
  }

  void Update() {
    HealthBar.transform.rotation = Quaternion.identity;
    HealthBar.transform.position = transform.position - healthBarCanvasOffset;

    if (PlayerTransform != null) {
      RotateTo(PlayerTransform.position);
      
      //implements out of squad enemy behaviour
      if ((float) Stats.CurrentHealth / Stats.MaxHealth < InSquadHealthBarrier)
        if ((PlayerTransform.position - transform.position).magnitude > 4) {
          Move(transform.up);
        }
        else {
          Move(-transform.right);
        }
      
    }

    if (PlayerTransform != null && reloadTimer <= 0.0f) {
      SpawnMissiles("EnemyShot");
      reloadTimer += Stats.ShootSpeed / 1000;
    }

    if (reloadTimer > 0.0f) {
      reloadTimer -= Time.deltaTime;
    }
  }

  private void OnTriggerExit2D(Collider2D other) {
    if (other.gameObject.CompareTag("GameArea")) {
      if (Squad != null) {
        Squad.Units.Remove(this);
        Squad = null;
      }

      Destroy(gameObject);
      GameController.Instance.CheckSquad();
    }
  }

  public override void Die() {
    base.Die();

    if (Squad != null) {
      Squad.Units.Remove(this);
      Squad = null;
    }

    GameController.Instance.Score += Stats.MaxHealth * (1 + Stats.Mobility / 100); //just a sample score bonus
    GameHUDController.Instance.SetScoreValue(GameController.Instance.Score);
    Destroy(gameObject);
    GameController.Instance.CheckSquad();

    var bonuses = DataController.Instance.BonusesConfig.Bonuses;
    Instantiate(bonuses[Random.Range(0, bonuses.Count)].Prefab, transform.position, Quaternion.identity);
  }
}