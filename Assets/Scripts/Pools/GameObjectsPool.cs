﻿using UnityEngine;
using System.Collections.Generic;

public class GameObjectsPool : MonoBehaviour
{
  public GameObject Prefab;
  public int InitialCount = 16;

  private Stack<GameObject> pooledObjects;

  void Start() {
    pooledObjects = new Stack<GameObject>(InitialCount);
    for (int i = 0; i < InitialCount; i++) {
      GameObject obj = Instantiate(Prefab, transform);
      obj.SetActive(false);
      pooledObjects.Push(obj);
    }
  }

  public GameObject Pop() {
    if (pooledObjects.Count > 0) {
      return pooledObjects.Pop();
    }

    GameObject obj = Instantiate(Prefab, transform);
    return obj;
  }

  public void Push(GameObject obj) {
    pooledObjects.Push(obj);
  }

  /// <summary>
  /// Used for deleting all redundant object on the level start
  /// </summary>
  public void PushAllActiveChilds() {
    
    for (int i = 0; i < gameObject.transform.childCount; i++) {
      var child = gameObject.transform.GetChild(i);
      if (child.gameObject.activeInHierarchy) {
        child.gameObject.SetActive(false);
        Push(child.gameObject);
      }
    }
  }
  
}