﻿using System.Collections;
using UnityEngine;

public class ExplosionsPool : GameObjectsPool
{
  public static ExplosionsPool Instance;

  void Awake() {
    if (!Instance) Instance = this;
  }

  public void PushAfterSeconds(GameObject explosion, float time) {
    StartCoroutine(PushToPool(explosion, time));
  }
  
  IEnumerator PushToPool(GameObject explosion, float time) {
    yield return new WaitForSeconds(time);
    Instance.Push(explosion);
    explosion.SetActive(false);
  }
}